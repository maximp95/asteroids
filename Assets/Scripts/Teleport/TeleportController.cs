﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TeleportController : MonoBehaviour
{

    public event Action<NameSide> PlayerOnDorder;
    public event Action<NameSide> BulletOnDorder;

    public enum NameSide { Top, Bottom, Right, Left, Bullet }
    protected SpriteRenderer spriteRend;
    protected Vector2 setBoundaries;
    protected Transform m_Transform;
    


    private void Awake()
    {
        m_Transform = transform; 
        spriteRend = GetComponent<SpriteRenderer>();
        setBoundaries = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0)); //создание гарниц по краям камеры


    }
    private void Update()
    {
        sideTrig(); // определение переченного барьера (пули отдельно)
    }

    public void sideTrig()
    {
        if (gameObject.tag != "Bullet" && m_Transform.position.x >= setBoundaries.x)
            PlayerOnDorder(NameSide.Right);

        else if (gameObject.tag != "Bullet" && m_Transform.position.x <= -setBoundaries.x)
            PlayerOnDorder(NameSide.Left);

        if (gameObject.tag != "Bullet" && m_Transform.position.y >= setBoundaries.y)
            PlayerOnDorder(NameSide.Top);

        else if (gameObject.tag != "Bullet" && m_Transform.position.y <= -setBoundaries.y)
            PlayerOnDorder(NameSide.Bottom);

        if (gameObject.tag == "Bullet" && (transform.position.x >= setBoundaries.x || transform.position.x <= -setBoundaries.x || transform.position.y >= setBoundaries.y || transform.position.y <= -setBoundaries.y))
        {
            BulletOnDorder(NameSide.Bullet);
        }
        
    } // определение переченного барьера (пули отдельно)
}
