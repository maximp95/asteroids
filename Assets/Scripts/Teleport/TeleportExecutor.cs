﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TeleportExecutor : TeleportController
{
   
    private void Start()
    {
        PlayerOnDorder += Teleport;
        BulletOnDorder += BulletOff;
    } // подписки

    
    private void Teleport(NameSide side)
    {
        switch (side)
        {
            case NameSide.Top:
                transform.position -= new Vector3(0, setBoundaries.y * 2, 0);
                break;

            case NameSide.Bottom:
                transform.position -= new Vector3(0, -setBoundaries.y * 2, 0);
                break;

            case NameSide.Right:
                transform.position -= new Vector3(setBoundaries.x * 2, 0, 0);
                break;

            case NameSide.Left:
                transform.position -= new Vector3(-setBoundaries.x * 2, 0, 0);
                break;

            case NameSide.Bullet:
                gameObject.SetActive(false);
                break;
        }
        
    } //обработчик событий при пересечении ганиц экрана
    private void BulletOff(NameSide name)
    {
       
        gameObject.SetActive(false);
    } //отключение пули из пула

    private void OnDestroy()
    {
        PlayerOnDorder -= Teleport;
        BulletOnDorder -= BulletOff;
    } //отписки
}
