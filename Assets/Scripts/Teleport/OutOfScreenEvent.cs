﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;


public class OutOfScreenEvent : MonoBehaviour
{
    public delegate void SideContact(NameSide side);
    public event SideContact  PlayerOnDorder;
    public enum NameSide { Top, Bottom, Right, Left }
    protected SpriteRenderer spriteRend;
    protected Vector2 setBoundaries;
    

    private void Awake()
    {
        spriteRend = GetComponent<SpriteRenderer>();
        setBoundaries = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
    }
    private void Update()
    {
        sideTrig();
    }
    public void Teleport(NameSide side)
    {
        switch (side)
        {
            case NameSide.Top:
                transform.position -= new Vector3(0, setBoundaries.y * 2, 0);
                break;

            case NameSide.Bottom:
                transform.position -= new Vector3(0, -setBoundaries.y * 2, 0);
                break;

            case NameSide.Right:
                transform.position -= new Vector3(setBoundaries.x * 2, 0, 0);
                break;

                case NameSide.Left:
                transform.position -= new Vector3(-setBoundaries.x * 2, 0, 0);
                break;
        }
    }
    public void sideTrig ()
    {
        if (transform.position.x >= setBoundaries.x)
            PlayerOnDorder(NameSide.Right);

        else if (transform.position.x <= -setBoundaries.x)
            PlayerOnDorder(NameSide.Left);

        if (transform.position.y >= setBoundaries.y)
            PlayerOnDorder(NameSide.Top);

        else if (transform.position.y <= -setBoundaries.y)
            PlayerOnDorder(NameSide.Bottom);
    }
}
