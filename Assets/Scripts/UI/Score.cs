﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Score : MonoBehaviour
{
    [SerializeField] private Text scorePlayer, highScorePlayer;
    [SerializeField] private Transform asteroidPool;
    static int counteScore, counreHighScore;
    private float _second = 0;
    private Asteroid _asteroid;

    private void Awake()
    {
        _asteroid = asteroidPool.GetChild(0).GetComponent<Asteroid>();
        //_asteroid.AsteroidBonusScore += BonusForAsteroid;

        if (PlayerPrefs.HasKey("SaveHighScore"))
        {
            counreHighScore = PlayerPrefs.GetInt("SaveHighScore");
        }
        
    }
    void Update()
    {
        RecordCounter();
    }

    
    public void RecordCounter()
    {
        _second += Time.deltaTime * 4;
        counteScore = (int)Mathf.Round(_second);

        counteScore += Mathf.RoundToInt(0.1f);

        if (counteScore >= counreHighScore)
        {
            counreHighScore = counteScore;
            PlayerPrefs.GetInt("SaveHighScore", counreHighScore);
        }

        scorePlayer.text = $"Score: " + counteScore;
        highScorePlayer.text = $"High score: " + counreHighScore;

        
    } //счетчик счета

}
