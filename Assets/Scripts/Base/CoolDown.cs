﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoolDown : MonoBehaviour
{
    public Image iconCoolDown;
    [SerializeField] private Image iconReady;
    public bool coolingDown;
    [SerializeField][Range(0,30)] private float waitTimeCoolDownLaser = 15.0f;
    public bool laserStop = false;


    public void CooldownLaser()
    {

        coolingDown = true;
        StartCoroutine(Cooldown());
        iconReady.gameObject.SetActive(false);
    }
    void Update()
    {
        if (coolingDown == true)
        {
            iconCoolDown.fillAmount -= 1.0f / waitTimeCoolDownLaser * Time.deltaTime;
        }
    }
    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(waitTimeCoolDownLaser);
        laserStop = false;
        coolingDown = false;
        iconReady.gameObject.SetActive(true);
        iconCoolDown.fillAmount = 1f;

    }
}
