﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShoot : MonoBehaviour

{
    [SerializeField] private Transform bulletPoolTransform;
    [SerializeField] private Transform bulletSpawnLeft;
    [SerializeField] private Transform bulletSpawnRight;

    protected Rigidbody bullet;
    protected int currentBulletIndex = 0;

    public void ShootLeft()
    {
        //Debug.Log("2");
        bullet = bulletPoolTransform.GetChild(currentBulletIndex).GetComponent<Rigidbody>();
        bullet.transform.position = bulletSpawnLeft.position;
        bullet.gameObject.SetActive(true);
        bullet.velocity = Vector3.zero;
        bullet.rotation = bulletSpawnLeft.rotation;
        bullet.AddRelativeForce(Vector2.up * 10, ForceMode.Impulse);
        currentBulletIndex++;

        if (currentBulletIndex >= bulletPoolTransform.childCount)
            currentBulletIndex = 0;
    }
    public void ShootRight()
    {
        //Debug.Log("1");
        bullet = bulletPoolTransform.GetChild(currentBulletIndex).GetComponent<Rigidbody>();
        bullet.transform.position = bulletSpawnRight.position;
        bullet.gameObject.SetActive(true);
        bullet.velocity = Vector3.zero;
        bullet.rotation = bulletSpawnLeft.rotation;
        bullet.AddRelativeForce(Vector2.up * 10, ForceMode.Impulse);
        currentBulletIndex++;

        if (currentBulletIndex >= bulletPoolTransform.childCount)
            currentBulletIndex = 0;
    }

    public void ShootActive()
    {
        bullet = bulletPoolTransform.GetChild(currentBulletIndex).GetComponent<Rigidbody>();
        bullet.transform.position = bulletSpawnRight.position;
        bullet.gameObject.SetActive(true);
        bullet.velocity = Vector3.zero;
        bullet.rotation = bulletSpawnLeft.rotation;
        bullet.AddRelativeForce(Vector2.up * 10, ForceMode.Impulse);
        currentBulletIndex++;

        if (currentBulletIndex >= bulletPoolTransform.childCount)
            currentBulletIndex = 0;
    }
}
