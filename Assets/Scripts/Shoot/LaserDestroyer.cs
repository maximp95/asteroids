﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserDestroyer : MonoBehaviour
{
    [SerializeField] private Transform asteroidPoolTransforfm;
    [SerializeField] private GameObject prefabMesAsteroid;
    [SerializeField] [Range(0, 3)] private float activeTimeLaser = 0.5f;




    [SerializeField] private CoolDown coolDownScr;
    public enum BonusScore { asteroidBones };

    protected Rigidbody2D asteroid;
    [SerializeField] private static int countAsteroids = 7;
    protected static int indexPoolAsteroids;


    public void LaserActive()
    {
        gameObject.SetActive(true);
        StartCoroutine(LaserActiveTimer());
    }

    IEnumerator LaserActiveTimer()
    {
        coolDownScr.laserStop = true;
        yield return new WaitForSeconds(activeTimeLaser);
        transform.gameObject.SetActive(false);
        coolDownScr.CooldownLaser();
        
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Asteroid" || collision.tag == "SmallAsteroid") // контакт пули
        {
            if (collision.tag == "SmallAsteroid")
            {
                collision.gameObject.SetActive(false); // отключение астероида
                countAsteroids--;
            }


            if (collision.tag == "Asteroid") //спаун маленьких астероидов из пула астороидов
            {
                Destroy(collision.gameObject); // отключение астероида
                countAsteroids--;

                for (int i = 0; i < 3; i++)
                {
                    asteroid = asteroidPoolTransforfm.GetChild(indexPoolAsteroids).GetComponent<Rigidbody2D>();
                    asteroid.transform.position = collision.gameObject.transform.position;
                    asteroid.gameObject.SetActive(true);
                    indexPoolAsteroids++;


                    if (indexPoolAsteroids >= asteroidPoolTransforfm.childCount)
                        indexPoolAsteroids = 0;
                }

            }
            if (countAsteroids < 6)
            {
                Instantiate(prefabMesAsteroid, collision.transform.position, Quaternion.identity);
                countAsteroids += 4;
            }
        }
    }
}
   

