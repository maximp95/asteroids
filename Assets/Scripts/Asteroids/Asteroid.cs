﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Asteroid : MonoBehaviour
{
    private Rigidbody2D rbAsteroid;

    [SerializeField] [Range(0, 1000)] private float maxThrust; 
    [SerializeField] [Range(0, 100)] private float maxTorque;
    public event Action AsteroidBonusScore = delegate { };

    void Start()
    {
        rbAsteroid = GetComponent<Rigidbody2D>();

        Vector2 thrust = new Vector2(UnityEngine.Random.Range(maxThrust, -maxThrust), UnityEngine.Random.Range(maxThrust, -maxThrust));
        float tourque = UnityEngine.Random.Range(maxTorque, -maxTorque); 

        rbAsteroid.AddForce(thrust); //движение астероида
        rbAsteroid.AddTorque(tourque); //вращеине астероида
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            AsteroidBonusScore();
        }
        if (collision.tag == "Player")
        {
            Invoke("RestartLevel", 0.2f);
        }
    } //столкнвоение с астероидом

    private void RestartLevel()
    {
        SceneManager.LoadScene("Asteroids");
    }
    
}
