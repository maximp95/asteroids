﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DestroyComponent : MonoBehaviour
{
    [SerializeField] private Transform asteroidPoolTransforfm;
    [SerializeField] private GameObject prefabMesAsteroid;
    [SerializeField] private GameObject spawnPointAsteroid;
    [SerializeField] private GameObject targetGameObj;
    private Rigidbody2D asteroidRb2D;
    public enum BonusScore {asteroidBones};
    
    protected Rigidbody2D asteroid;
    [SerializeField] private static int countAsteroids = 7;
    protected static int indexPoolAsteroids;




    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Asteroid" || collision.tag == "SmallAsteroid") // контакт пули
        {
            if (collision.tag == "SmallAsteroid")
            {
                collision.gameObject.SetActive(false); // отключение астероида
                gameObject.SetActive(false); // отключение пули
                countAsteroids--;
            }
            
            
            if (collision.tag == "Asteroid") //спаун маленьких астероидов из пула астороидов
            {
                Destroy(collision.gameObject); // отключение астероида
                gameObject.SetActive(false); // отключение пули
                countAsteroids--;

                for (int i = 0; i < 3; i++)
                {
                    asteroid = asteroidPoolTransforfm.GetChild(indexPoolAsteroids).GetComponent<Rigidbody2D>();
                    asteroid.transform.position = collision.gameObject.transform.position;
                    asteroid.gameObject.SetActive(true);
                    indexPoolAsteroids++ ;
                    

                    if (indexPoolAsteroids >= asteroidPoolTransforfm.childCount)
                        indexPoolAsteroids = 0;
                }
                
            }
            if (countAsteroids < 11)
            {
               GameObject medAster =  Instantiate(prefabMesAsteroid, spawnPointAsteroid.transform.position, Quaternion.identity);
                asteroidRb2D = medAster.GetComponent<Rigidbody2D>();
                asteroidRb2D.AddForce(Vector2.up);

                countAsteroids += 4;
            }
        }
    }
    
}
   

