﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MoveController : MonoBehaviour
{
    protected Rigidbody2D rb2D;
    [SerializeField] [Range(0, 1000)] private float enginePower;
    [SerializeField] [Range(0, 50)] private float engineReversPower;
    [SerializeField] [Range(0, 1000)] private float rotateSpeed;
    [SerializeField] [Range(0, 1)] private float resistance; //сопротивление,  как в классической игре

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        rb2D.AddForce(rb2D.velocity * - resistance, ForceMode2D.Force);
    }

    public void engineOperation()
    {
        rb2D.AddForce(transform.up * enginePower * Time.deltaTime, ForceMode2D.Force);
    }
    public void engineReverse()
    {
        rb2D.AddForce(- transform.up * engineReversPower, ForceMode2D.Force);
    }

    public void RotateShipRight()
    {
        
        transform.Rotate(0, 0, -1 * rotateSpeed * Time.deltaTime);
    }
    public void RotateShipLeft()
    {
        
        transform.Rotate(0, 0, 1 * rotateSpeed * Time.deltaTime);
    }
}
