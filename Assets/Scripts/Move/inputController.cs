﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inputController : MonoBehaviour
{
    [SerializeField] private KeyCode keyCodeEngine;
    [SerializeField] private KeyCode keyCodeEngineRevers;
    [SerializeField] private KeyCode keyCodeLeft;
    [SerializeField] private KeyCode keyCodeRight;
    [SerializeField] private KeyCode keyCodeShootBullet;
    [SerializeField] private KeyCode keyCodeShootLaser;
    [SerializeField] private Transform bulletPoolTransform;
    

    [SerializeField] private Transform bulletSpawnLeft;
    [SerializeField] private Transform bulletSpawnRight;

   

    private MoveController _moveController;
    protected Rigidbody2D bullet;
    protected int currentBulletIndex = 0; //индекс пули из пула
    private Rigidbody2D rbSpawn;
    [SerializeField] private LaserDestroyer laserOblect;
    [SerializeField] private CoolDown coolDownScr;
    

    private void Awake()
    {
        _moveController = GetComponent<MoveController>();
        rbSpawn = bulletSpawnLeft.GetComponent<Rigidbody2D>();


    }
  
   private void Update()
    {
        inputMove();
        Shoot();
    }
    public void inputMove()
    {
        if (Input.GetKey(keyCodeEngine))
        {
            _moveController.engineOperation();
        }
        else if (Input.GetKey(keyCodeEngineRevers))
        {
            _moveController.engineReverse();
        }

        if (Input.GetKey(keyCodeLeft))
        {
            _moveController.RotateShipLeft();
        }
        else if (Input.GetKey(keyCodeRight))
        {
            _moveController.RotateShipRight();
        }
    }
    public void Shoot()
    {
        if (Input.GetKeyDown(keyCodeShootBullet))
        {
            ShootLeft();
            ShootRight();

        }

        if (Input.GetKeyDown(keyCodeShootLaser))
            if (!coolDownScr.laserStop)
            {

              laserOblect.LaserActive();

            }
    } //стрельба

    private void ShootLeft()
    {
        bullet = bulletPoolTransform.GetChild(currentBulletIndex).GetComponent<Rigidbody2D>();
        bullet.transform.position = bulletSpawnLeft.position;
        bullet.gameObject.SetActive(true);
        bullet.velocity = Vector3.zero;
        bullet.rotation = rbSpawn.rotation;
        bullet.AddRelativeForce(Vector2.up * 400);
        currentBulletIndex++;

        if (currentBulletIndex >= bulletPoolTransform.childCount)
            currentBulletIndex = 0;
    }
    private void ShootRight()
    {
       bullet = bulletPoolTransform.GetChild(currentBulletIndex).GetComponent<Rigidbody2D>();
       bullet.transform.position = bulletSpawnRight.position;
       bullet.gameObject.SetActive(true);
       bullet.velocity = Vector3.zero;
       bullet.rotation = rbSpawn.rotation;
       bullet.AddRelativeForce(Vector2.up * 400);
       currentBulletIndex++;

       if (currentBulletIndex >= bulletPoolTransform.childCount)
            currentBulletIndex = 0;
    }

}
